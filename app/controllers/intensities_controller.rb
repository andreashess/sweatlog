class IntensitiesController < ApplicationController
  before_action :set_intensity, only: [:show, :edit, :update, :destroy]

  def index
    @intensities = Intensity.all
    respond_with(@intensities)
  end

  def show
    respond_with(@intensity)
  end

  def new
    @intensity = Intensity.new
    respond_with(@intensity)
  end

  def edit
  end

  def create
    @intensity = Intensity.new(intensity_params)
    @intensity.save
    respond_with(@intensity)
  end

  def update
    @intensity.update(intensity_params)
    respond_with(@intensity)
  end

  def destroy
    @intensity.destroy
    respond_with(@intensity)
  end

  private
    def set_intensity
      @intensity = Intensity.find(params[:id])
    end

    def intensity_params
      params.require(:intensity).permit(:name)
    end
end
