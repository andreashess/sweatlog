class LogbooksController < ApplicationController
  before_action :set_logbook, only: [:show, :edit, :update, :destroy]

  def index
    @logbooks = Logbook.all
    respond_with(@logbooks)
  end

  def show
    respond_with(@logbook)
  end

  def new
    @logbook = Logbook.new
    respond_with(@logbook)
  end

  def edit
  end

  def create
    @logbook = Logbook.new(logbook_params)
    @logbook.save
    respond_with(@logbook)
  end

  def update
    @logbook.update(logbook_params)
    respond_with(@logbook)
  end

  def destroy
    @logbook.destroy
    respond_with(@logbook)
  end

  private
    def set_logbook
      @logbook = Logbook.find(params[:id])
    end

    def logbook_params
      params.require(:logbook).permit(:year, :club_id, :user_id)
    end
end
