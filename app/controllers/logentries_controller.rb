class LogentriesController < ApplicationController
  before_action :set_logentry, only: [:show, :edit, :update, :destroy]

  def index
    lb = Logbook.where(user: current_user)
    @logentries = Logentry.where(logbook: lb )
    respond_with(@logentries)
  end

  def show
    respond_with(@logentry)
  end

  def new
    lb = Logbook.where(user: current_user).first

	if ( lb == nil )
	  lb = Logbook.new()
	  lb.year = '2014-01-01 00:00:00'
	  lb.club = Club.where(name: 'See-Club Zug').first
	  lb.user = current_user
	  lb.save
	end


	sp = Sport.where(name: 'Rudern').first	
	inten = Intensity.where(name: 'ga1').first

    @logentry = Logentry.new(:logbook_id => lb.id,:sport_id =>sp.id,:intensity_id => inten.id )
    respond_with(@logentry)
  end

  def edit
  end

  def create
    @logentry = Logentry.new(logentry_params)
    @logentry.save
    respond_with(@logentry)
  end

  def update
    @logentry.update(logentry_params)
    respond_with(@logentry)
  end

  def destroy
    @logentry.destroy
    respond_with(@logentry)
  end

  private
    def set_logentry
      @logentry = Logentry.find(params[:id])
    end

    def logentry_params
      params.require(:logentry).permit(:start, :logbook_id, :sport_id, :intensity_id, :runtime, :distance, :avg_heart_rate, :lactat)
    end
end
