class Logentry < ActiveRecord::Base
  belongs_to :logbook
  belongs_to :sport
  belongs_to :intensity

  validates_presence_of :start
  validates_presence_of :sport
  validates_presence_of :intensity


  validates :runtime , numericality: { greater_than_or_equal_to: 0, allow_nil: true}
  validates :distance , numericality: { greater_than_or_equal_to: 0, allow_nil: true }
  validates :lactat , numericality: { greater_than_or_equal_to: 0, allow_nil: true }
  validates :avg_heart_rate , numericality: { greater_than_or_equal_to: 0, allow_nil: true }
end
