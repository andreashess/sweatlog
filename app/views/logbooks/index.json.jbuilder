json.array!(@logbooks) do |logbook|
  json.extract! logbook, :id, :year, :club_id, :user_id
  json.url logbook_url(logbook, format: :json)
end
