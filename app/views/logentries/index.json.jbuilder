json.array!(@logentries) do |logentry|
  json.extract! logentry, :id, :start, :logbook_id, :sport_id, :intensity_id, :runtime, :distance, :avg_heart_rate, :lactat
  json.url logentry_url(logentry, format: :json)
end
