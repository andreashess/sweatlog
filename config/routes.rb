Rails.application.routes.draw do
  resources :logentries

  resources :logbooks

  resources :intensities

  resources :sports

  resources :clubs

  root to: 'logentries#index'
  devise_for :users
  resources :users
end
