class CreateLogbooks < ActiveRecord::Migration
  def change
    create_table :logbooks do |t|
      t.date :year
      t.references :club, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
