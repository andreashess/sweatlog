class CreateLogentries < ActiveRecord::Migration
  def change
    create_table :logentries do |t|
      t.timestamp :start
      t.references :logbook, index: true
      t.references :sport, index: true
      t.references :intensity, index: true
      t.float :runtime
      t.integer :distance
      t.integer :avg_heart_rate
      t.integer :lactat

      t.timestamps
    end
  end
end
