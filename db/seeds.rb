# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = CreateAdminService.new.call
puts 'CREATED ADMIN USER: ' << user.email

Sport.delete_all
Intensity.delete_all


Sport.create(name: 'Rudern')
Sport.create(name: 'Ergometer')
Sport.create(name: 'Laufen')
Sport.create(name: 'Radfahren')
Sport.create(name: 'Kraft')
Sport.create(name: 'Hallentraining')
Sport.create(name: 'Alternativtraining')
 
 
Intensity.create(name: 'ga0')
Intensity.create(name: 'ga1')
Intensity.create(name: 'ga2')
Intensity.create(name: 'as')
Intensity.create(name: 'intensiv')
Intensity.create(name: 'maximal ( Ausdauer ) ')
Intensity.create(name: 'Ausdauer')
Intensity.create(name: 'Hypertrophie')
Intensity.create(name: 'intramuskuläre Koordination')
Intensity.create(name: 'Plyometrie (Kraft)')







