require 'test_helper'

class LogentriesControllerTest < ActionController::TestCase
  setup do
    @logentry = logentries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:logentries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create logentry" do
    assert_difference('Logentry.count') do
      post :create, logentry: { avg_heart_rate: @logentry.avg_heart_rate, distance: @logentry.distance, intensity_id: @logentry.intensity_id, lactat: @logentry.lactat, logbook_id: @logentry.logbook_id, runtime: @logentry.runtime, sport_id: @logentry.sport_id, start: @logentry.start }
    end

    assert_redirected_to logentry_path(assigns(:logentry))
  end

  test "should show logentry" do
    get :show, id: @logentry
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @logentry
    assert_response :success
  end

  test "should update logentry" do
    patch :update, id: @logentry, logentry: { avg_heart_rate: @logentry.avg_heart_rate, distance: @logentry.distance, intensity_id: @logentry.intensity_id, lactat: @logentry.lactat, logbook_id: @logentry.logbook_id, runtime: @logentry.runtime, sport_id: @logentry.sport_id, start: @logentry.start }
    assert_redirected_to logentry_path(assigns(:logentry))
  end

  test "should destroy logentry" do
    assert_difference('Logentry.count', -1) do
      delete :destroy, id: @logentry
    end

    assert_redirected_to logentries_path
  end
end
