#!/bin/sh
unicorn_pid=`cat /var/www/sweatlog/pids/unicorn.pid`
echo "Restarting Unicorn ($unicorn_pid)"
kill -HUP $unicorn_pid
