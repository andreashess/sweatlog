#!/bin/sh
unicorn_pid=`cat /var/www/sweatlog/pids/unicorn.pid`
echo "Stopping Unicorn ($unicorn_pid)"
kill -9 $unicorn_pid
